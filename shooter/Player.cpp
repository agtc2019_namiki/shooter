/* ===================================================== *
	FILE : Player.cpp
	NAME : Y.Yamashita

 + ------ Explanation of file --------------------------
       
	Playerクラスの実装部

 * ===================================================== */
#include "Common.h"

/* ----------------------------------------------------- *
			コンストラクタ
 * ----------------------------------------------------- */
Player::Player(){
	init() ;
}

/* ----------------------------------------------------- *
			  デストラクタ
 * ----------------------------------------------------- */
Player::~Player(){
}

/* ----------------------------------------------------- *
			  初期セット
 * ----------------------------------------------------- */
void Player::init( ){
	SetHitCapcule( PLAYER_WIDTH, PLAYER_HEIGHT, m_pos ) ;		// 初期ヒットカプセル
	m_move = VGet( 0.0f, 0.0f, 0.0f ) ;							// 初期移動量
	m_direction = eDown ;										// 初期角度
	m_playtime = 0.0f ;											// アニメ進行時間変数
	m_playspd = 1.5f ;											// アニメ進行速度変数
	m_jmpflg = FALSE ;											// ジャンプフラグ
	m_runningflg = FALSE ;										// 走りフラグ
	m_animmode = eStand ;										// 初期立ち
	m_killscore = 0 ;											// 初期キルスコア
	m_dropscore = 0 ;											// 初期落下スコア
}

/* ----------------------------------------------------- *
		モデルのロードと初期座標のセット
 * ----------------------------------------------------- */
int Player::LoadModel( int selectmodel )
{
	switch( selectmodel )
	{
		case PC_1 :
			// --- モデル初期セット
			m_model = MV1LoadModel(PLAYER_PATH"IwatobiPengin.mv1") ;
			// --- モデル大きさ調整
			MV1SetScale(m_model, VGet(20.0f, 20.0f, 20.0f)) ;
			break ;

		case PC_2 :
			m_model = MV1LoadModel(PLAYER_PATH"KingPengin.mv1") ;
			// --- モデル大きさ調整
			MV1SetScale(m_model, VGet(20.0f, 20.0f, 20.0f)) ;
			break ;

		case PC_3 :
			// --- モデル初期セット
			m_model = MV1LoadModel(PLAYER_PATH"MIlitaryPengin.mv1") ;
			// --- モデル大きさ調整
			MV1SetScale(m_model, VGet(20.0f, 20.0f, 20.0f)) ;
			break ;
	}

	// --- アニメーション初期セット
	m_anim[eStand] = MV1LoadModel(PLAYER_PATH"PlayerStand.mv1") ;
	m_anim[eRun] = MV1LoadModel(PLAYER_PATH"PlayerWalk.mv1") ;
	m_anim[eAttack] = MV1LoadModel(PLAYER_PATH"PlayerAttack.mv1") ;
	m_anim[eDamage] = MV1LoadModel(PLAYER_PATH"PenginDamage.mv1") ;
	m_attachidx = MV1AttachAnim( m_model, 0, m_anim[eStand]) ;
	m_anim_totaltime = MV1GetAttachAnimTotalTime( m_model, m_attachidx ) ;

	// --- アニメーションの補正
	m_rootflm = MV1SearchFrame( m_model, "Master" ) ;	// アニメーションで移動をしているフレームの番号を検索する
	MV1SetFrameUserLocalMatrix( m_model, m_rootflm, MGetIdent() ) ;	// アニメーションで移動をしているフレームを無効にする

	return (false);
}


/* ----------------------------------------------------- *
					アクション
				引数：g_key
 * ----------------------------------------------------- */
int Player::Action( unsigned short key ){
	// --- キー情報入力
	KeyCheck( key ) ;						//	キーチェック
	// --- ヒットチェック
	HitCheck( ) ;
	// --- プレイヤーの移動
	PosMove( m_move.x, m_move.y, m_move.z ) ;
	// --- 描画処理
	DrawModel( );

	return (false) ;
}

/* ----------------------------------------------------- *
					モデルの描画
 * ----------------------------------------------------- */
int Player::DrawModel( )
{
	Animation( ) ;							//	アニメーション
	MV1SetPosition( m_model, m_pos );		//	座標セット
	MV1SetRotationXYZ( m_model, VGet(0.0f, 0.78f * m_direction, 0.0f)) ;	//	180°= 1πラジアン = 3.14f、90°= 1/2πラジアン = 1.57f
	MV1DrawModel( m_model );				//	描画処理

	return (false) ;
}

/* ----------------------------------------------------- *
					キー入力をチェック
		引数：key
		key情報を受け取り、ビット演算を行い
		それぞれアクションを起こす
 * ----------------------------------------------------- */
int Player::KeyCheck( unsigned short key ){
	float root_harf = (1.41421356237f / 2.0f) ;		// 斜め移動の減少値
	// --- 入力待機時
	m_move.z = 0.0f ;
	m_move.x = 0.0f ;

	// --- アニメフラグがオフなら動く
	if ( m_animflg == FALSE ){
	// --- 上下左右キー
		// --- 上入力
		if(key & PAD_UP){
			m_move.x = 0.0f ;
			m_move.z = PLAYER_ZSPD_UP ;
			m_direction = eUp ;
		}
		// --- 下入力
		if(key & PAD_DOWN){
			m_move.x = 0.0f ;
			m_move.z = PLAYER_ZSPD_DOWN ;
			m_direction = eDown ;
		}
		// --- 左入力
		if(key & PAD_LEFT){
			m_move.x = PLAYER_XSPD_LEFT ;
			m_move.z = 0.0f ;
			m_direction = eLeft ;
		}
		// --- 右入力
		if(key & PAD_RIGHT){
			m_move.x = PLAYER_XSPD_RIGHT ;
			m_move.z = 0.0f ;
			m_direction = eRight ;
		}
		// --- 上左入力
		if((key & PAD_UP) && (key & PAD_LEFT)){
			m_move.x = (PLAYER_XSPD_LEFT * root_harf) ;
			m_move.z = (PLAYER_ZSPD_UP * root_harf) ;
			m_direction = eUpLeft ;
		}
		// --- 上右入力
		if((key & PAD_UP) && (key & PAD_RIGHT)){
			m_move.x = (PLAYER_XSPD_RIGHT * root_harf) ;
			m_move.z = (PLAYER_ZSPD_UP * root_harf) ;
			m_direction = eUpRight ;
		}

		// --- 下左入力
		if((key & PAD_DOWN) && (key & PAD_LEFT)){
			m_move.x = (PLAYER_XSPD_LEFT * root_harf) ;
			m_move.z = (PLAYER_ZSPD_DOWN * root_harf) ;
			m_direction = eDownLeft ;
		}

		// --- 下右入力
		if((key & PAD_DOWN) && (key & PAD_RIGHT)){
			m_move.x = (PLAYER_XSPD_RIGHT * root_harf) ;
			m_move.z = (PLAYER_ZSPD_DOWN * root_harf) ;
			m_direction = eDownRight ;
		}

	// --- ボタンキー
		// --- 〇入力
		if(key & PAD_A){
printfDx("〇入力\n") ;
		}

		// --- ×入力
		if(key & PAD_B){
printfDx("×入力\n") ;
		}

		// --- □入力
		if(key & PAD_Y){
printfDx("□入力\n") ;
		}

		// --- △入力
		if(key & PAD_X){
printfDx("△入力\n") ;
		}

		// --- R1入力
		if(key & PAD_R1){
printfDx("R1入力\n") ;
		}

		// --- R2入力
		if(key & PAD_R2){
printfDx("R2入力\n") ;
		}

		// --- L1入力
		if(key & PAD_L1){
printfDx("L1入力\n") ;
		}

		// --- L2入力
		if(key & PAD_L2){
printfDx("L2入力\n") ;
		}

		// --- START入力
		if(key & PAD_START){
printfDx("START入力\n") ;
		}

		// --- SELECT入力
		if(key & PAD_SELECT){
printfDx("SELECT入力\n") ;
		}
	}

	AnimKeyCheck( key ) ;
	return( false ) ;
}
/* ----------------------------------------------------- *
					キー入力のアニメ処理
 * ----------------------------------------------------- */
int Player::AnimKeyCheck( unsigned short key ) {
	// --- eStand キー入力がされていなければ
	if ( (key == 0) && (m_animflg == FALSE) ){
		AnimationModeChange( eStand ) ;
	}

	// --- eRun	上下左右キー
	if( (key & PAD_UP) || (key & PAD_RIGHT)
	 || (key & PAD_DOWN) || (key & PAD_LEFT) ){
		 if ( m_animflg == FALSE ){
			AnimationModeChange( eRun ) ;
		 }
	}

	// --- eAttack Mキーが入力されたとき
	if( (CheckHitKey(KEY_INPUT_M) > 0) && (m_atkflg == FALSE) 
	 && (m_animflg == FALSE) ){
		AnimationModeChange( eAttack ) ;
	}

	return (false) ;
}


/* ----------------------------------------------------- *
						ヒットチェック
			第一引数：チェックしたい相手のpos
			第二引数：チェックしたい相手のmove
			第三引数：チェックしたい相手のwidth
 * ----------------------------------------------------- */
int Player::HitCheck( ){



	return (false) ;
}

/* ----------------------------------------------------- *
			  移動関数
		引数１：x移動量
		引数２：y移動量
		引数３：z移動量
 * ----------------------------------------------------- */
int Player::PosMove( float x, float y, float z ){
	m_pos.x += x ;		// プレイヤーのX座標に移動量
	m_pos.y += y ;		// プレイヤーのY座標に移動量
	m_pos.z += z ;		// プレイヤーのZ座標に移動量

	return (false) ;
}

/* ----------------------------------------------------- *
					アニメーション
 * ----------------------------------------------------- */
int Player::Animation(){

//	if ( (m_playtime < m_anim_totaltime) || (m_animmode != eJumpLoop) ){
	if (m_playtime <= m_anim_totaltime){
		//	アニメーション進行を行う
		m_playtime += m_playspd ;
	}
	//	進行時間が総時間を上回ったら、0にする
	if ( m_playtime > m_anim_totaltime ){
		m_playtime = 0.0f ;		//　プレイタイムをリセット
		// --- アニメフラグが立っていたら
		if (m_animflg == TRUE){
			AnimStopAction() ;		//	アニメ終了後処理
			m_animflg = FALSE ;		//	アニメフラグを折る
		}
	}
	MV1SetAttachAnimTime( m_model, m_attachidx, m_playtime ) ;	//	アニメーション進行時間と同期させる

	return( false ) ;
}

/* ----------------------------------------------------- *
				アニメーションモードチェンジ
			第一引数：ActionModeの定数
 * ----------------------------------------------------- */
int Player::AnimationModeChange( int anim_mode ){
	m_animmode = anim_mode ;
	//	アニメをデタッチする
	MV1DetachAnim( m_model, m_attachidx ) ;
	//	アニメをアタッチする
	m_attachidx = MV1AttachAnim( m_model, 0, m_anim[m_animmode]) ;
	//	アニメーションの総時間を取得する
	m_anim_totaltime = MV1GetAttachAnimTotalTime( m_model, m_attachidx) ;

	switch( m_animmode ){
		case eStand :	// --- 移動しない
			m_move.x = 0.0f ;
			m_move.z = 0.0f ;
			break ;

		case eRun :		// --- 走る
			break ;

		case eAttack :	// --- 移動しない
			m_move.x = 0.0f ;
			m_move.y = 0.0f ;
			m_move.z = 0.0f ;
			m_animflg = TRUE ;
			m_atkflg = TRUE ;
			m_playtime = 0.0f ;
			break ;
	}

	return (false) ;
}

/* ----------------------------------------------------- *
		特定のアニメーション後に処理をする関数
 * ----------------------------------------------------- */
int Player::AnimStopAction(){
	switch ( m_animmode ){
		case eAttack :
			m_atkflg = FALSE ;		//	攻撃フラグを折る
			break ;
	}

	return (false) ;
}


/* ----------------------------------------------------- *
			  座標セット
			  mposに座標をセット
		引数１：x座標
		引数２：y座標
		引数３：z座標
 * ----------------------------------------------------- */
int Player::SetPos( float x, float y, float z ){
	m_pos = VGet( x, y, z ) ;

	return (false) ;
}

/* ----------------------------------------------------- *
			  ヒットチェックカプセルセット
			  mposに座標をセット
		引数１：width		幅
		引数２：height		高さ
		引数３：centerPos	中心点
 * ----------------------------------------------------- */
int Player::SetHitCapcule( float arg_a, float arg_b, VECTOR arg_c ){
	m_width = arg_a ;	
	m_height = arg_b ;
	m_centerPos = arg_c ;

	return (false) ;
}


