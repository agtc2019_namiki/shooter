/* ===================================================== *
	FILE : Player.h
	NAME : Y.Yamashita

 + ------ Explanation of file --------------------------
       
	Playerクラスの宣言部

 * ===================================================== */
class Player {
	public :
		Player() ;			//	コンストラクタ
		~Player() ;			//	デストラクタ
		virtual void init() ;		//	初期セット

		// --- モデル描画関数
		virtual int LoadModel( int );						// 仮想関数 モデル読みこみ
		int DrawModel();									// モデル描画
		// --- アクション関数
		virtual int Action( unsigned short key );			// 仮想関数 アクション
		int KeyCheck( unsigned short key ) ;				// キー入力をチェック
		int HitCheck() ;									// ヒットチェック
		int PosMove( float x, float y, float z ) ;			// 移動関数
		// --- アニメ関数
		int AnimKeyCheck( unsigned short key ) ;			// キー入力のアニメ処理
		int Animation() ;									// アニメーション
		virtual int AnimationModeChange( int anim_mode ) ;			// 仮想関数 アニメーションモードチェンジ
		virtual int AnimStopAction() ;								// 仮想関数特定のアニメーション後に処理をする関数
		// --- セット関数
		int SetPos( float x, float y, float z ) ;			//	セットm_pos	
		int SetHitCapcule( float arg_a, float arg_b, VECTOR arg_c ) ;		//	セットヒットチェックカプセル	

		// --- モデル変数
		int m_model ;										//	モデルハンドル
		int m_action_no ;									//	アクションナンバー
		VECTOR m_pos ;										//	ベクトルの座標(float型座標)
		VECTOR m_move ;										//	移動量
		unsigned short m_key ;								//	キー情報
		int	m_direction ;									//	回転角度
		// --- ヒットチェック変数
		float m_width ;										//	当たり判定の幅
		float m_height ;									//	当たり判定の高さ
		VECTOR m_centerPos ;								//	当たり判定の中心座標
		// --- アニメ変数
		int m_anim[MAX_PLAYER_ANIM] ;						//	プレイヤーのアニメハンドル
		int m_attachidx ;									//	アタッチインデックスを格納
		int m_animmode ;									//	アニメモード
		float m_anim_totaltime ;							//	アニメ総時間格納変数
		float m_playtime ;									//	アニメ進行時間変数
		float m_playspd ;									//	アニメ進行速度変数
		int m_rootflm ;										//	ルートフレーム
		// --- アニメーションフラグ
		int m_jmpflg ;										//	ジャンプフラグ
		int m_animflg ;										//	アニメーション中フラグ
		int m_atkflg ;										//	攻撃フラグ
		int m_runningflg ;									//	走りフラグ

		// --- その他変数
		int m_killscore ;									//	叩いた回数
		int m_dropscore ;									//	落ちた回数

} ;

