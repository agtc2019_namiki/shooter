/* ===================================================== *
	FILE : Object.cpp
	NAME : K.Sakurai

 + ------ Explanation of file --------------------------
       
	Objectクラスの実装部

 * ===================================================== */
#include "Common.h"

/* ----------------------------------------------------- *
			コンストラクタ
 * ----------------------------------------------------- */
Object::Object(){
	init() ;
}

/* ----------------------------------------------------- *
			  デストラクタ
 * ----------------------------------------------------- */
Object::~Object(){
}

/* ----------------------------------------------------- *
			  初期セット
 * ----------------------------------------------------- */
void Object::init( ){
}

/* ----------------------------------------------------- *
			モデルのロードと初期座標のセット
 * ----------------------------------------------------- */
int Object::LoadModel( int selectmodel )
{
	switch( selectmodel )
	{
		case eSky :
			m_model = MV1LoadModel( "..\\Data\\Object\\skydome.mv1" ) ;
			break ;

		case eBlock :
			// --- モデルのパス登録
			m_model = MV1LoadModel( "..\\Data\\Object\\ice.mv1" ) ;
			break ;

		case eWall :
//			m_model = MV1LoadModel( "..\\Data\\Object\\ice.mv1" ) ;
			break ;

		case eIce :
//			m_model = MV1LoadModel( "..\\Data\\Object\\ice.mv1" ) ;
			break ;

		case eIceberg :
			m_model = MV1LoadModel( "..\\Data\\Object\\iceberg.mv1" ) ;
			break ;

		case eIcicles :
//			m_model = MV1LoadModel( "..\\Data\\Object\\ice.mv1" ) ;
			break ;

		case eSea :
			m_model = MV1LoadModel( "..\\Data\\Object\\Seamodifire03.mv1" ) ;
			break ;
	}

	return (false);
}

/* ----------------------------------------------------- *
			モデルのコピーと初期座標のセット
 * ----------------------------------------------------- */
int Object::CopyModel( int selectmodel )
{
	switch ( selectmodel ){
		// --- 床
		case eBlock :
			m_model = MV1DuplicateModel( g_object[HEAD_BLOCK].m_model ) ;
			break ;

		// --- 壁
		case eWall :
			m_model = MV1DuplicateModel( g_object[HEAD_WALL].m_model ) ;
			break ;

		// --- 氷塊
		case eIce :
			m_model = MV1DuplicateModel( g_object[HEAD_ICE].m_model ) ;
			break ;

		// --- 氷山
		case eIceberg :
			m_model = MV1DuplicateModel( g_object[HEAD_ICEBERG].m_model ) ;
			break ;

		// --- 氷柱
		case eIcicles :
			m_model = MV1DuplicateModel( g_object[HEAD_ICICLES].m_model ) ;
			break ;
	}

	return (false) ;
}

/* ----------------------------------------------------- *
					モデルの描画
 * ----------------------------------------------------- */
int Object::DrawModel( )
{
	Animation( ) ;							//	アニメーション
	MV1SetPosition( m_model, m_pos );		//	座標セット
	MV1DrawModel( m_model );				//	描画処理

	return (false) ;
}


/* ----------------------------------------------------- *
					アニメーション
 * ----------------------------------------------------- */
int Object::Animation(){

//	if ( (m_playtime < m_anim_totaltime) || (m_animmode != eJumpLoop) ){
	if (m_playtime <= m_anim_totaltime){
		//	アニメーション進行を行う
		m_playtime += m_playspd ;
	}
	//	進行時間が総時間を上回ったら、0にする
	if ( m_playtime > m_anim_totaltime ){
		m_playtime = 0.0f ;
		m_animflg = FALSE ;
		AnimStopAction() ;		//	アニメ終了後処理
	}
	MV1SetAttachAnimTime( m_model, m_attachidx, m_playtime ) ;	//	アニメーション進行時間と同期させる

	return( false ) ;
}


/* ----------------------------------------------------- *
		特定のアニメーション後に処理をする関数
 * ----------------------------------------------------- */
int Object::AnimStopAction(){
/*	switch ( m_animmode ){

	}
*/
	return (false) ;
}


/* ----------------------------------------------------- *
			  座標セット
			  mposに座標をセット
		引数１：x座標
		引数２：y座標
		引数３：z座標
 * ----------------------------------------------------- */
int Object::SetPos( float x, float y, float z ){
	m_pos = VGet( x, y, z ) ;

	return (false) ;
}

