/* ===================================================== *
	FILE : Camera.cpp
	NAME : Y.Yamashita

 + ------ Explanation of file --------------------------
       
	Cameraクラスの実装部

 * ===================================================== */
#include "Common.h"

/* ----------------------------------------------------- *
			コンストラクタ
 * ----------------------------------------------------- */
Camera::Camera(){
	init() ;
}

/* ----------------------------------------------------- *
			  デストラクタ
 * ----------------------------------------------------- */
Camera::~Camera(){
}

/* ----------------------------------------------------- *
			  初期セット
 * ----------------------------------------------------- */
void Camera::init(){
	//	カメラ座標変換
//	SetCPos( g_player.m_pos.x, g_player.m_pos.y + 200.0f, g_player.m_pos.z - 200.0f ) ;
//	SetCTgt( g_player.m_pos.x, g_player.m_pos.y + 145.0f, g_player.m_pos.z - 5.0f ) ;
	SetCPos( m_cpos.x = 236.0f, m_cpos.y = 240.0f, m_cpos.z = -224.0f ) ;
	SetCTgt( m_ctgt.x = 236.0f, m_ctgt.y = 136.0f, m_ctgt.z = -29.0f ) ;
	SetCUp( 0.0f, 0.0f, 1.0f ) ;
}
/* ------------------------------------------------------------------------- *
								デバッグキー いつか消す

 * ------------------------------------------------------------------------- */
void DebugKey( ){
// --- カメラ操作
	if (GetKeyState( 'A' ) < 0){		// --- A情報
		g_camera.m_cpos.x -= 4.0f ;
		g_camera.m_ctgt.x -= 4.0f ;
	}
	if (GetKeyState( 'D' ) < 0){		// --- D情報
		g_camera.m_cpos.x += 4.0f ;
		g_camera.m_ctgt.x += 4.0f ;
	}
	if (GetKeyState( 'W' ) < 0){		// --- W情報
		g_camera.m_cpos.z += 4.0f ;
		g_camera.m_ctgt.z += 4.0f ;
	}
	if (GetKeyState( 'S' ) < 0){		// --- S情報
		g_camera.m_cpos.z -= 4.0f ;
		g_camera.m_ctgt.z -= 4.0f ;
	}

	if (GetKeyState( 'Q' ) < 0){		// --- Q情報
		g_camera.m_cpos.y += 4.0f ;
		g_camera.m_ctgt.y += 4.0f ;
	}
	if (GetKeyState( 'E' ) < 0){		// --- E情報
		g_camera.m_cpos.y -= 4.0f ;
		g_camera.m_ctgt.y -= 4.0f ;
	}
	if (GetKeyState( 'C' ) < 0){		// --- C情報
		g_camera.m_ctgt.y += 4.0f ;
	}
	if (GetKeyState( 'Z' ) < 0){		// --- Z情報
		g_camera.m_ctgt.y -= 4.0f ;
	}


	if (GetKeyState( 'X' ) < 0){		// --- X情報	カメラ情報
		printf("m_cpos.x = %f, m_cpos.y = %f, m_cpos.z = %f\n", g_camera.m_cpos.x, g_camera.m_cpos.y, g_camera.m_cpos.z) ;
		printf("m_ctgt.x = %f, m_ctgt.y = %f, m_ctgt.z = %f\n", g_camera.m_ctgt.x, g_camera.m_ctgt.y, g_camera.m_ctgt.z) ;
	}
}

/* ----------------------------------------------------- *
						アクション
 * ----------------------------------------------------- */
int Camera::Action(){
	//	カメラ座標変換
/*	SetCPos( g_player.m_pos.x, g_player.m_pos.y + 200.0f, g_player.m_pos.z - 200.0f ) ;
	SetCTgt( g_player.m_pos.x, g_player.m_pos.y + 145.0f, g_player.m_pos.z - 5.0f ) ;
	SetCUp( 0.0f, 0.0f, 1.0f ) ;
*/
//デバッグ --- キー入力
DebugKey( ) ;

	SetCUp( 0.0f, 0.0f, 1.0f ) ;
//	SetCameraPositionAndTargetAndUpVec( m_cpos, m_ctgt, m_cup ) ;
	SetCameraPositionAndTarget_UpVecY( m_cpos, m_ctgt ) ;

	return (false) ;
}

/* ----------------------------------------------------- *
			  セット視点
			  m_cposに座標をセット
		引数１：x座標
		引数２：y座標
		引数３：z座標
 * ----------------------------------------------------- */
int Camera::SetCPos( float x, float y, float z ){
	m_cpos = VGet( x, y, z ) ;		//	カメラの位置を格納
	return (false) ;
}

/* ----------------------------------------------------- *
			  セット注視点
			  m_ctgtに座標をセット
		引数１：x座標
		引数２：y座標
		引数３：z座標
 * ----------------------------------------------------- */
int Camera::SetCTgt( float x, float y, float z ){
	m_ctgt = VGet( x, y, z ) ;		//	カメラの位置を格納
	return (false) ;
}

/* ----------------------------------------------------- *
			  セット上方向
			  m_cupに座標をセット
		引数１：x座標
		引数２：y座標
		引数３：z座標
 * ----------------------------------------------------- */
int Camera::SetCUp( float x, float y, float z ){
	m_cup = VGet( x, y, z ) ;		//	カメラの位置を格納
	return (false) ;
}



