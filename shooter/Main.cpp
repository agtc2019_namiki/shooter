/* +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ *
							FILE: Main.cpp
							NAME: Y.N
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ */
// --- 共通ファイル
#include "Common.h"

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ *

								メイン関数

 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

int WINAPI WinMain( HINSTANCE hI,HINSTANCE hP,LPSTR lpC,int nC ){

	// --- ウィンドウモードの切り替え
//	ChangeWindowMode( FALSE ) ;		// フルスク表示
	ChangeWindowMode( TRUE ) ;

	// --- ウィンドウサイズの変更
//	SetGraphMode( 1920,980,32 ) ;	// フルスク表示
	SetGraphMode( WINDOW_WIDTH, WINDOW_HEIGHT, 32 ) ;

	// --- DXライブラリの初期化
	if( DxLib_Init() == -1 ) return -1 ;

	// --- 裏画面の描画の設定
	SetDrawScreen( DX_SCREEN_BACK ) ;

	// --- モデル初期化
	g_object[HEAD_SKY].LoadModel( eSky ) ;
	g_object[HEAD_BLOCK].LoadModel( eBlock ) ;
	g_object[HEAD_WALL].LoadModel( eWall ) ;
	g_object[HEAD_ICE].LoadModel( eIce ) ;
	g_object[HEAD_SEA].LoadModel( eSea ) ;
	g_object[HEAD_ICEBERG].LoadModel( eIceberg ) ;
	g_object[HEAD_ICICLES].LoadModel( eIcicles ) ;
	// --- モデルのコピー
	for ( int i = 0 ; i < MAX_OBJECT ; i++ ){
		if ( (i > HEAD_BLOCK) && (i < (HEAD_BLOCK + MAX_BLOCK)) ){
			g_object[i].CopyModel( eBlock ) ;
		}
	}
			//	後でduplicateに変える
	g_player_1P.LoadModel( PC_1 ) ;		// モデル読みこみ
	g_player_2P.LoadModel( PC_3 ) ;		// モデル読みこみ
			//	後でduplicateに変える
	g_npc[0].LoadModel( eNPCReferee) ;		// レフェリー
	g_npc[1].LoadModel( eNPCBlue ) ;		// 青
	g_npc[2].LoadModel( eNPCBlue ) ;		// 青
	g_npc[3].LoadModel( eNPCRed ) ;			// 赤
	g_npc[4].LoadModel( eNPCRed ) ;			// 赤
	g_npc[5].LoadModel( eNPCGreen ) ;		// 緑
	g_npc[6].LoadModel( eNPCGreen ) ;		// 緑

	// 初期座標セット
	g_npc[0].SetPos( 250.0f,   0.0f, 300.0f ) ;		// レフェリー
	g_npc[1].SetPos(   0.0f,   0.0f,   0.0f ) ;		// 青
	g_npc[2].SetPos(   0.0f,   0.0f, 100.0f ) ;		// 青
	g_npc[3].SetPos(   0.0f,   0.0f, 200.0f ) ;		// 赤
	g_npc[4].SetPos( 500.0f,   0.0f,   0.0f ) ;		// 赤
	g_npc[5].SetPos( 500.0f,   0.0f, 100.0f ) ;		// 緑
	g_npc[6].SetPos( 500.0f,   0.0f, 200.0f ) ;		// 緑

	g_player_1P.SetPos( 100.0f, PLAYER_YPOSITION, 0.0f ) ;
	g_player_2P.SetPos( 400.0f, PLAYER_YPOSITION, 0.0f ) ;
	// --- ===============仮================== -------------------------------------
	sceneselect = eSceneTitle ;

	// --- メッセージ内部
	while( ProcessMessage() == 0 && CheckHitKey(KEY_INPUT_ESCAPE) == 0 ){

		// --- 画面の消去
		ClearDrawScreen() ;

		// --- scene分け
		SceneLoop( ) ;

		// --- 表画面と裏画面の切り替え
		ScreenFlip() ;
	}
	// --- DXライブラリの終了処理
	DxLib_End() ;
	return 0 ;
}


