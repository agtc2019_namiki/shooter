/*
				< 流れや処理、使用関数などのプログラムメモを各自記入してください >

								< 全体メモ(編集可) >
			ファイル {
				<.cpp>
				Main.cpp
				Common.cpp
				ActionLoop.cpp
				Player.cpp
				Camera.cpp

				<.h>
				Common.h
				Player.h
				Camera.h
			}

			関数(Mainループ) {
				ClearDrawScreen() ;
				ScreenFlip( ) ;
				ActionLoop( ) ;
			}

			クラス化 {
				Camera	→ g_camera
				メンバ変数
		
				Player	→ g_player
				メンバ変数
					m_key		// キー情報格納
				
			}

			enum {
				// シーンの登録
				ScenName
					eSceneBlank = -1 ,		←強制終了用

				// プレイヤーの方向
				PlDirection

				// プレイヤーの状態
				PlAnimation

			}

			構造体 {
				今後実装予定
			}

			プロトタイプ宣言 {
				int ActionLoop( void ) ;
				int ClearCheck( void ) ;
				int FlipScreen( void ) ;
			}

								< 個人メモ(各自記入) >
	双木 <メイン>
		Main.cpp
			基本的に3つの関数だけでメインの処理を行う。
			変更は基本なし。
			・メインループ使用関数
				ClearDrawScreen( ) ;			// クリア処理
				ScreenFlip( ) ;					// 
				ActionLoop( ) ;					// 基本的な動作のループ処理
			・define関係
				WINDOW_WIDTH			// ウィンドウ関係		
				WINDOW_HEIGHT

		Common.cpp
			・オブジェクトの実体宣言		
			g_camera ;		Cameraクラス
			g_player ;		Playerクラス

		Common.h
			define宣言
			ヘッダーファイル登録


		モデルの管理
		model		= プレイヤー
		player_arm	= 武器
		obj			= オブジェクト




*/
