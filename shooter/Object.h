/* ===================================================== *
	FILE : Object.h
	NAME : K.Sakurai

 + ------ Explanation of file --------------------------
       
	Objectクラスの宣言部

 * ===================================================== */

class Object {
	public :
		Object() ;			//	コンストラクタ
		~Object() ;			//	デストラクタ
		void init() ;		//	初期セット

		// --- モデル描画関数
		int LoadModel( int );								// モデル読みこみ
		int CopyModel( int );								// モデルコピー
		int DrawModel();									// モデル描画
		// --- アニメ関数		
		int Animation() ;									//	アニメーション
		int AnimStopAction() ;								//	特定のアニメーション後に処理をする関数
		// --- アクション関数
		int Action();								//	仮想関数 アクション 
		// --- セット関数
		int SetPos( float x, float y, float z ) ;			//	セットm_pos	
		int SetHitCapcule( float arg_a, float arg_b, VECTOR arg_c ) ;		//	セットヒットチェックカプセル	

		// --- モデル変数
		int m_model ;										//	モデルハンドル
		int m_action_no ;									//	アクションナンバー
		VECTOR m_pos ;										//	ベクトルの座標(float型座標)
		VECTOR m_move ;										//	移動量
		unsigned short m_key ;								//	キー情報
		int	m_direction ;									//	回転角度
		// --- ヒットチェック変数
		float m_width ;										//	当たり判定の幅
		float m_height ;									//	当たり判定の高さ
		VECTOR m_centerPos ;								//	当たり判定の中心座標
		// --- アニメ変数
		int m_anim[MAX_OBJECT_ANIM] ;						//	アニメハンドル
		int m_attachidx ;									//	アタッチインデックスを格納
		int m_animmode ;									//	アニメモード
		float m_anim_totaltime ;							//	アニメ総時間格納変数
		float m_playtime ;									//	アニメ進行時間変数
		float m_playspd ;									//	アニメ進行速度変数
		int m_rootflm ;										//	ルートフレーム
		// --- アニメーションフラグ
		int m_jmpflg ;										//	ジャンプフラグ
		int m_animflg ;										//	アニメーション中フラグ
		int m_atkflg ;										//	攻撃フラグ
		int m_runningflg ;									//	走りフラグ
		// --- 
		int AttachFrameNum ;	// アタッチするフレームの番号
		MATRIX FrameMatrix ;	// フレームマトリクス

} ;

//　武器のモデルを特定のフレームと連動させるプログラム
//int WeponAttachFrameNum ;		// 特定のフレーム番号を格納する変数
//MATRIX WeponFrameMatrix ;		// 武器のフレームマトリクスを格納する変数
/* ----------------------------------------- *
    MV1SearchFrame()
	特定のフレームを探索する関数
	返り値:特定のフレーム番号
	引数1 :特定のオブジェクト
	引数2 :特定のフレーム(ボーン名)
 * ----------------------------------------- */
	//WeponAttachFrameNum = MV1SearchFrame(g_player.m_model, "Finger_R") ;

/* ----------------------------------------- *
    MV1GetFrameLocalWorldMatrix()
	武器をアタッチするフレームのローカル→ワールド変換行列を取得する
	返り値:武器のマトリクス
	引数1 :モデルハンドル
	引数2 :特定のフレーム番号
 * ----------------------------------------- */
	//WeponFrameMatrix = MV1GetFrameLocalWorldMatrix(g_player.m_model, WeponAttachFrameNum);

// --- マトリクスをモデルハンドルをセットする
	//MV1SetMatrix(model[2], WeponFrameMatrix);

/* ================= *
	備考
	MV1GetFrameLocalWorldMatrix()以降は
	多分毎フレームごとに呼び出す必要有りかも。未検証
	回転角度はお好みで
 * ================= */

