/* ===================================================== *
	FILE : SceneLoop.cpp
	NAME : Y.Namiki

 + ------ Explanation of file --------------------------
       
	SceneLoop関数の実装部

 * ===================================================== */
// --- 共通ファイル
#include "Common.h"

int SceneLoop( ) {
	switch( sceneselect ) {
		// --- タイトル画面
		case eSceneTitle :
			sceneselect++ ;
			break ;

		// --- ロード画面
		case eSceneLoad :
			InitSkydome( ) ;		// 空の初期化
			InitBlock( ) ;			// 床の初期化
			InitIceberg( ) ;
			InitSea( ) ;			// 海の初期化
			SetMapData( ) ;			// マップデータの初期化
//			LookMapData( ) ;		// マップデータの閲覧(コンソール表示)
			sceneselect++ ;
			break ;

		// --- メイン処理
		case eSceneMain :
			ActionLoop( ) ;		// アクションループ
			break ;

		// --- 終了処理
		case eSceneOver :
			break ;

		// --- ブランク
		case eSceneBlank :		// -1
			break ;
	}

	return ( false ) ;
}

