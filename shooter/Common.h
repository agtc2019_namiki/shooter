/* +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ *
							FILE: Common.cpp
							NAME: Y.N
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ */

/* -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|
|       define宣言
|
* -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ */
#define WINDOW_WIDTH		1200
#define WINDOW_HEIGHT		 675

// --- プレイヤーセレクト(パス)
#define PC_1 		    0
#define PC_2		    1 
#define PC_3		    2

// --- プログラム用define
#define MAX_PATH_CHAR  100
#define DATA_PATH	"..\\Data\\"						// データパス
#define PLAYER_PATH DATA_PATH"..\\Data\\Player\\"		// プレイヤーパス
#define OBJECT_PATH DATA_PATH"..\\Data\\Object\\"		// オブジェクトパス
#define KID_PATH	DATA_PATH"NPC\\Kids\\"				// NPC(キッズ)パス
#define YUNOMI_PATH	DATA_PATH"NPC\\Yunomi\\"			// NPC(ゆのみ)パス

// --- キー情報(unsigned short)
#define KEY_UP		0x0001
#define KEY_DOWN	0x0002
#define KEY_LEFT	0x0004
#define KEY_RIGHT	0x0008
// --- 〇×△□の仮define
#define KEY_W		0x0010
#define KEY_A		0x0020
#define KEY_S		0x0040
#define KEY_D		0x0080

// --- マップデータ
#define MAP_X_MAX					11				// マップデータの最大X
#define MAP_Y_MAX					 8				// マップデータの最大Y
#define MAP_X_MIN					 0				// マップデータの最小X
#define MAP_Y_MIN					 0				// マップデータの最小Y

#define MAP_OFFSET_X_MAX			10				// マップデータの最大X(オフセット時)
#define MAP_OFFSET_Y_MAX			 7				// マップデータの最大Y(オフセット時)
#define MAP_OFFSET_X_MIN			 1				// マップデータの最小X(オフセット時)
#define MAP_OFFSET_Y_MIN			 1				// マップデータの最小Y(オフセット時)

// --- オブジェクト情報
#define MAX_OBJECT			500				// オブジェクトの最大数
#define MAX_OBJECT_PATH		 20				// オブジェクトのパス最大数
#define MAX_OBJECT_ANIM		  2				// アニメーション数
#define HEAD_SKY			  2				// 空の頭番号
#define HEAD_SEA			  5				// 海の頭番号
#define HEAD_HAMMER			 40				// ハンマーの頭番号
#define MAX_HAMMER			  5				// ハンマー最大数
#define HEAD_BLOCK			 50				// 足場の頭番号
#define MAX_BLOCK			100				// 足場の最大数
#define HEAD_WALL			150				// 壁の頭番号
#define MAX_WALL			 50				// 壁の最大数
#define HEAD_ICE			200 			// 氷塊の頭番号
#define MAX_ICE				 10				// 氷塊の最大数
#define HEAD_ICEBERG		210				// 足場の頭番号
#define MAX_ICEBERG			  5				// 氷山の最大数
#define HEAD_ICICLES		215				// 足場の頭番号
#define MAX_ICICLES			 50				// 氷柱の最大数

// --- プレイヤー情報
#define MAX_PLAYER_PATH		  10				// プレイヤーのパス最大数
#define PLAYER_GRAVITY		0.5f				// 重力
#define PLAYER_YPOSITION	0.0f				// プレイヤーY座標
#define PLAYER_WIDTH		60.0f				// プレイヤーサイズ(幅)
#define PLAYER_HEIGHT		300.0f				// プレイヤーサイズ(高さ)
#define MAX_PLAYER_ANIM		6					// プレイヤーのアニメハンドル
	// --- プレイヤースピード
#define PLAYER_XSPD_LEFT	-6.0f				// 	←(x)			プレイヤースピード
#define PLAYER_XSPD_RIGHT	6.0f				//	→(x)			プレイヤースピード
#define PLAYER_ZSPD_DOWN	PLAYER_XSPD_LEFT	//	↓(y)			プレイヤースピード
#define PLAYER_ZSPD_UP		PLAYER_XSPD_RIGHT	//	↑(y)			プレイヤースピード
#define PLAYER_YSPD			15.0f				//	↑(z)			プレイヤースピード

// --- NPC情報
#define MAX_NPC_PATH		 20				// NPCのパス数
#define MAX_NPC				  7				// NPCの最大数
#define MAX_NPC_ANIM		  5				// 各NPCの最大アニメーション数

// --- キーパッドdefine
#define PAD_UP					PAD_INPUT_UP			// ↑
#define PAD_DOWN				PAD_INPUT_DOWN			// ↓
#define PAD_LEFT				PAD_INPUT_LEFT			// ←
#define PAD_RIGHT				PAD_INPUT_RIGHT			// →
#define PAD_A					PAD_INPUT_B				// 〇
#define PAD_B					PAD_INPUT_C				// ×
#define PAD_Y					PAD_INPUT_X				// □
#define PAD_X					PAD_INPUT_A				// △
#define PAD_R1					PAD_INPUT_R				// R1
#define PAD_R2					PAD_INPUT_Z				// R2
#define PAD_L1					PAD_INPUT_L				// L1
#define PAD_L2					PAD_INPUT_Y				// L2
#define PAD_START				PAD_INPUT_M				// START
#define PAD_SELECT				PAD_INPUT_START			// SELECT

// 海用の設定-----------------------------------------------------------------------
#define SEA_ALPHA				   190					// 海の透明度
#define WAVESPEED				  0.01f					// シェイプが動く速さ 0.0f〜1.0fで
#define SEAHEIGHT				-200.0f					// 海の位置を下げる数値(ステージは-0.0f)
#define SEAROTATIONSPEED		  0.04f					// 海が回転する速さ

// debug用変数-----------------------------------------------------------------------
#define MAX_ICE_X	9
#define MAX_ICE_Y	6

/* -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|
|		共通ヘッダー
|
* -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ */
#include <DxLib.h>
#include "Camera.h"
#include "Player.h"
#include "ConsoleWindow.h"
#include "Object.h"
#include "NPC.h"

/* -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|
|		型宣言
|
* -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ */
// --- シーン情報
enum SceneName {
	eSceneBlank = -1 ,
	eSceneTitle ,
	eSceneLoad ,
	eSceneMain ,
	eSceneOver ,
} ;

// --- モデル角度の情報
enum PlDirection {
	eDown,					// ↓
	eDownLeft,				// 
	eLeft,					// ←		<<<< ==== >>>>
	eUpLeft,				//			<<<< 方向 >>>>
	eUp,					// ↑		<<<< ==== >>>>
	eUpRight,				// 
	eRight,					// →
	eDownRight				// 
} ;

// --- プレイヤーの状態
enum PlAnimation {
	eStand,			// 立ち
	eRun,			// 走り
	eAttack,		// 攻撃
	eDamage			// ダメージ
} ;

// --- オブジェクトの種類
enum OBName {
	eSky,				// 空
	eBlock,				// 足場
	eWall,				// 壁
	eIce,				// 氷塊
	eIceberg,			// 氷山
	eIcicles,			// 氷柱
	eSea,				// 海
	eHammer				// ハンマー
} ;

// --- NPCの種類
enum NPCName {
	eNPCReferee,		// 審判ペンギン
	eNPCBlue,			// 青ペンギン
	eNPCRed,			// 赤ペンギン
	eNPCGreen			// 緑ペンギン
} ;

// --- NPCの状態
enum NPCAnimation {	
	eRefereeStand,		// 審判ペンギン立ち
	eRefereeLeftWin,	// 審判ペンギン左勝利
	eRefereeRightWin,	// 審判ペンギン右勝利
	eNPCStand,			// 立ち
	eNPCSit,			// 座り
} ;

/* -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|
|		プロトタイプ宣言
|
* -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ */
int ActionLoop( void ) ;
int SceneLoop( void ) ;
int InitSkydome( void ) ;
int InitBlock( void ) ;
int InitIceberg( void ) ;
int GetLightColor( void ) ;
int GetModelColor( int ) ;
int DrawSkydome( void ) ;
int InitSea( void ) ;
int DrawSea( void ) ;
int SetMapData( void ) ;
int LookMapData( void ) ;

/* -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|
|		関数宣言
|
* -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ */

/* -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|
|		構造体宣言
|
* -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ */

/* -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|
|		extern(グローバル)宣言									 
|
* -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ */
extern short g_key1P ;
extern short g_key2P ;

extern int g_hitmap[MAP_Y_MAX][MAP_X_MAX] ;

/* -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|
|		外部参照宣言
|
* -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ */
extern Camera g_camera ;
extern Player			g_player_1P ;	// 1P player
extern Player			g_player_2P ;	// 2P player
extern NPC				g_npc[MAX_NPC] ;		// NPCオブジェクト
extern Object g_load_object[MAX_OBJECT_PATH] ;	// オブジェクトのロード用オブジェクト 
extern Object g_object[ MAX_OBJECT ] ;	// MAX_OBJECT 500
extern ConsoleWindow	g_consolewindow ;

extern int sceneselect ;

