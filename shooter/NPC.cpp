/* ===================================================== *
	FILE : NPC.cpp
	NAME : Y.Yamashita

 + ------ Explanation of file --------------------------
       
	NPCクラスの実装部

 * ===================================================== */
#include "Common.h"
/* ----------------------------------------------------- *
			コンストラクタ
 * ----------------------------------------------------- */
NPC::NPC(){
	init() ;
}

/* ----------------------------------------------------- *
			  デストラクタ
 * ----------------------------------------------------- */
NPC::~NPC(){
}

/* ----------------------------------------------------- *
			  初期セット
 * ----------------------------------------------------- */
void NPC::init( ){
	m_move = VGet( 0.0f, 0.0f, 0.0f ) ;							//	初期移動量
	m_direction = eDown ;										//	初期角度
	m_playtime = 0.0f ;											//	アニメ進行時間変数
	m_playspd = 0.5f ;											//	アニメ進行速度変数
}

/* ----------------------------------------------------- *
		モデルのロードと初期座標のセット
 * ----------------------------------------------------- */
int NPC::LoadModel( int selectmodel )
{
	int GraphHandle ;
	float Scale = 20.0f ;

	switch( selectmodel )
	{
		case eNPCReferee :
			m_model = MV1LoadModel(YUNOMI_PATH"Yunomi_base.mv1") ;
			MV1SetScale(m_model, VGet(Scale, Scale, Scale)) ;
			break ;

		case eNPCBlue :
			m_model = MV1LoadModel(KID_PATH"KidsPengin.mv1") ;
			GraphHandle = LoadGraph( KID_PATH"KidsPengin_Blue.png" );
			MV1SetTextureGraphHandle( m_model, 0, GraphHandle, false) ;
			MV1SetScale(m_model, VGet(Scale, Scale, Scale)) ;
			break ;

		case eNPCRed :
			m_model = MV1LoadModel(KID_PATH"KidsPengin.mv1") ;
			GraphHandle = LoadGraph( KID_PATH"KidsPengin_Pink.png" );
			MV1SetTextureGraphHandle( m_model, 0, GraphHandle, false) ;
			MV1SetScale(m_model, VGet(Scale, Scale, Scale)) ;
			break ;

		case eNPCGreen :
			m_model = MV1LoadModel(KID_PATH"KidsPengin.mv1") ;
			GraphHandle = LoadGraph( KID_PATH"KidsPengin_Green.png" );
			MV1SetTextureGraphHandle( m_model, 0, GraphHandle, false) ;
			MV1SetScale(m_model, VGet(Scale, Scale, Scale)) ;
			break ;
	}

	// --- アニメーション初期セット
	if (selectmodel == eNPCReferee){			//	審判ペンギンなら専用のアニメーションを。
		m_anim[eRefereeStand] = MV1LoadModel(YUNOMI_PATH"Yunomi_Stand.mv1") ;
		m_anim[eRefereeLeftWin] = MV1LoadModel(YUNOMI_PATH"Yunomi_LeftWin.mv1") ;
		m_anim[eRefereeRightWin] = MV1LoadModel(YUNOMI_PATH"Yunomi_RightWin.mv1") ;
		m_animmode = eRefereeStand ;	//	初期立ち
	}else if(selectmodel <= eNPCGreen){		// Green以下ならば共通のアニメーションを。
		m_anim[eNPCStand] = MV1LoadModel(KID_PATH"PlayerStand.mv1") ;
		m_animmode = eNPCStand ;		//	初期立ち
	}
	m_attachidx = MV1AttachAnim( m_model, 0, m_anim[m_animmode]) ;
	m_anim_totaltime = MV1GetAttachAnimTotalTime( m_model, m_attachidx ) ;

	// --- アニメーションの補正
	m_rootflm = MV1SearchFrame( m_model, "Master" ) ;	// アニメーションで移動をしているフレームの番号を検索する
	MV1SetFrameUserLocalMatrix( m_model, m_rootflm, MGetIdent() ) ;	// アニメーションで移動をしているフレームを無効にする

	return (false);
}

/* ----------------------------------------------------- *
			モデルのコピーと初期座標のセット
 * ----------------------------------------------------- */
int NPC::CopyModel( int selectmodel )
{
	return (false) ;
}

/* ----------------------------------------------------- *
					モデルの描画
 * ----------------------------------------------------- */
int NPC::DrawModel( )
{
	Animation( ) ;							//	アニメーション
	MV1SetPosition( m_model, m_pos );		//	座標セット
	MV1SetRotationXYZ( m_model, VGet(0.0f, 0.78f * m_direction, 0.0f)) ;	//	180°= 1πラジアン = 3.14f、90°= 1/2πラジアン = 1.57f
	MV1DrawModel( m_model );				//	描画処理

	return (false) ;
}
/* ----------------------------------------------------- *
					アクション
				引数：g_key
 * ----------------------------------------------------- */
int NPC::Action( ){

	// --- 描画処理
	DrawModel( );

	return (false) ;
}

/* ----------------------------------------------------- *
					アニメーション
 * ----------------------------------------------------- */
int NPC::Animation(){

	if (m_playtime <= m_anim_totaltime){
		//	アニメーション進行を行う
		m_playtime += m_playspd ;
	}
	//	進行時間が総時間を上回ったら、0にする
	if ( m_playtime > m_anim_totaltime ){
		m_playtime = 0.0f ;		//　プレイタイムをリセット
		// --- アニメフラグが立っていたら
		if (m_animflg == TRUE){
//			AnimStopAction() ;		//	アニメ終了後処理
			m_animflg = FALSE ;		//	アニメフラグを折る
		}
	}
	MV1SetAttachAnimTime( m_model, m_attachidx, m_playtime ) ;	//	アニメーション進行時間と同期させる

	return( false ) ;
}

/* ----------------------------------------------------- *
				アニメーションモードチェンジ
			第一引数：ActionModeの定数
 * ----------------------------------------------------- */
int NPC::AnimationModeChange( int anim_mode ){
	m_animmode = anim_mode ;
	//	アニメをデタッチする
	MV1DetachAnim( m_model, m_attachidx ) ;
	//	アニメをアタッチする
	m_attachidx = MV1AttachAnim( m_model, 0, m_anim[m_animmode]) ;
	//	アニメーションの総時間を取得する
	m_anim_totaltime = MV1GetAttachAnimTotalTime( m_model, m_attachidx) ;

/*	switch( m_animmode ){
		
	}
*/
	return (false) ;
}

/* ----------------------------------------------------- *
		特定のアニメーション後に処理をする関数
 * ----------------------------------------------------- */
int NPC::AnimStopAction(){
/*	switch ( m_animmode ){

	}
	*/
	return (false) ;
}

/* ----------------------------------------------------- *
			  座標セット
			  mposに座標をセット
		引数１：x座標
		引数２：y座標
		引数３：z座標
 * ----------------------------------------------------- */
int NPC::SetPos( float x, float y, float z ){
	m_pos = VGet( x, y, z ) ;

	return (false) ;

}

/* ----------------------------------------------------- *
			  移動関数
		引数１：x移動量
		引数２：y移動量
		引数３：z移動量
 * ----------------------------------------------------- */
int NPC::PosMove( float x, float y, float z ){
	m_pos.x += x ;		// プレイヤーのX座標に移動量
	m_pos.y += y ;		// プレイヤーのY座標に移動量
	m_pos.z += z ;		// プレイヤーのZ座標に移動量

	return (false) ;
}
