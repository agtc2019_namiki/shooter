/* ===================================================== *
	FILE : Camera.h
	NAME : Y.Yamashita

 + ------ Explanation of file --------------------------
       
	Cameraクラスの宣言部

 * ===================================================== */
class Camera {
	public :
		Camera() ;			//	コンストラクタ
		~Camera() ;			//	デストラクタ
		void init() ;		//	初期セット

		VECTOR m_cpos ;		//	カメラの位置を格納		モデルの位置に注意
		VECTOR m_ctgt ;		//	カメラの注視点を格納
		VECTOR m_cup  ;		//	カメラの上方向
		//	アクション
		int Action() ;
		int CurcleAction() ;							//	円運動
		//	セット関数
		int SetCPos( float x, float y, float z ) ;		//	セット視点（カメラの位置）	
		int SetCTgt( float x, float y, float z ) ;		//	セット注視点（カメラの見るところ）	
		int SetCUp( float x, float y, float z ) ;		//	セット上方向（カメラの向き）	

} ;
