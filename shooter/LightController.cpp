/* ===================================================== *
	FILE : LightController.cpp
	NAME : K.Sakurai

 + ------ Explanation of file --------------------------
       
	LightInit関数の実装部

 * ===================================================== */
#include "Common.h"

int LightInit( void ) {
	// --- 大域環境照明
	SetGlobalAmbientLight( GetColorF(0.8f, 0.8f, 0.8f, 0.0f) ) ;

	// --- ライトのパラメータ設定
	SetLightDifColor( GetColorF(1.0f, 1.0f, 1.0f, 0.0f) ) ;		// ディフューズ
	SetLightSpcColor( GetColorF(1.0f, 1.0f, 1.0f, 0.0f) ) ;		// スペキュラー
	SetLightAmbColor( GetColorF(0.8f, 0.8f, 0.8f, 0.0f) ) ;		// アンビエント

	return( false ) ;
}




