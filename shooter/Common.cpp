/* +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ *
							FILE: Common.cpp
							NAME: Y.N
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ */
#include <DxLib.h>
#include "Common.h"

/* -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|
|		オブジェクトの実体宣言
|
* -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ */
Camera			g_camera ;
Player			g_player_1P ;				// 1P player
Player			g_player_2P ;				// 2P player
NPC				g_npc[MAX_NPC] ;			// NPCオブジェクト
Object			g_load_object[MAX_OBJECT_PATH] ;	// オブジェクトのロード用オブジェクト 
Object			g_object[ MAX_OBJECT ] ;	// MAX_OBJECT 500
ConsoleWindow	g_consolewindow ;

/* -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|
|		キャラ数分(グローバル)宣言									 
|
* -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ */
short g_key1P ;
short g_key2P ;

int g_hitmap[MAP_Y_MAX][MAP_X_MAX] ;

int sceneselect ;

/* -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|
|		グローバル変数宣言									 
|
* -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ */

// ====================================== 後で分ける ============================================ //
	int SoftImageHandle ;				// ソフトイメージの作成
	int shape[10] ;						// シェイプ登録用
	int shapeno = 0 ;					// シェイプ番号
	float seashapestatus = 0.0f;		// 海のシェイプの有効状態を入れておく 0.0f〜1.0f
	float seamovespeed = 0.0f ;			// シェイプを動かすスピードを格納する
	float searotation = 0.0f;			// 海を回転させる
	float searotationspeed = 0.0f;		// 海を回転させるスピードを格納する
// =============================================================================================== //


/* -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|
|		グローバル関数宣言
|
* -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ */
void LoadInit(){
	// --- モデル読み込み
/*	for (int i = 0 ; i < MAX_OBJECT_PATH ; i++)
	for (int i = 0 ; i < 6 ; i++){
		g_load_object[i].LoadModel(i) ;
	}
	// --- モデルコピー
	for (int i = 0 ; i < 6 ; i++){
//		g_load_object[i].Duplicate(    ) ;
	}
*/
}

void Pad_Key_Check( void ) {
	g_key1P = GetJoypadInputState( DX_INPUT_PAD1 ) ;
	g_key2P = GetJoypadInputState( DX_INPUT_PAD2 ) ;
}

int InitSkydome( void ) {
	float size = 50.0f ;

	MV1SetScale( g_object[HEAD_SKY].m_model, VGet(size, size, size) ) ;
	MV1SetPosition( g_object[HEAD_SKY].m_model, VGet(0.0f, -1000.0f, 0.0f) ) ;

	return ( false ) ;
}

int DrawSkydome( void ) {
	SetUseLighting( FALSE ) ;
	SetUseZBuffer3D( TRUE ) ;
	MV1DrawModel( g_object[HEAD_SKY].m_model ) ;
	SetUseLighting( TRUE ) ;
	SetUseZBuffer3D( FALSE ) ;
	
	return ( false ) ;
}

int InitIceberg( void ) {
	float size = 10.0f ;

	MV1SetScale( g_object[HEAD_ICEBERG].m_model, VGet(size, size, size) ) ;
	g_object[HEAD_ICEBERG].SetPos( 250.0f, -200.0f, 1100.0f ) ;

	return ( false ) ;
}

int GetLightColor( void ) {
	COLOR_F lightDif ;
	COLOR_F lightSpc ;
	COLOR_F lightAmb ;

	lightDif = GetLightDifColor() ;
	lightSpc = GetLightSpcColor() ;
	lightAmb = GetLightAmbColor() ;

	printf( "Light\n" ) ;
	printf( "Dif : R = %f, G = %f, B = %f\n", lightDif.r, lightDif.g, lightDif.b ) ;
	printf( "Spc : R = %f, G = %f, B = %f\n", lightSpc.r, lightSpc.g, lightSpc.b ) ;
	printf( "Amb : R = %f, G = %f, B = %f\n\n", lightAmb.r, lightAmb.g, lightAmb.b ) ;

	return ( false ) ;
}

int SetLightColor( void ) {

	return ( false ) ;
}



int GetModelColor( int model ) {
	COLOR_F modelDif ;
	COLOR_F modelSpc ;
	COLOR_F modelAmb ;

	modelDif = MV1GetMaterialDifColor( model, 0 ) ;
	modelSpc = MV1GetMaterialSpcColor( model, 0 ) ;
	modelAmb = MV1GetMaterialAmbColor( model, 0 ) ;

	printf( "Model\n" ) ;
	printf( "Dif : R = %f, G = %f, B = %f\n", modelDif.r, modelDif.g, modelDif.b ) ;
	printf( "Spc : R = %f, G = %f, B = %f\n", modelSpc.r, modelSpc.g, modelSpc.b ) ;
	printf( "Amb : R = %f, G = %f, B = %f\n\n", modelAmb.r, modelAmb.g, modelAmb.b ) ;

	return ( false ) ;
}

int InitSea( void ) {
	// --- 海の回転角度を格納する
	searotation = 0.0f ;

	// --- 乗算済みアルファの設定
//	SetUseBackBufferTransColorFlag( TRUE ) ;				// TRUEにするとキャプションバーが表示されないのでコメントアウト
	// --- shapeのフラグを有効に
	MV1SetUseShapeFlag( g_object[HEAD_SEA].m_model, TRUE ) ;
	// モデルの描画時にRGB値をA値で乗算する設定にする
	MV1SetUseDrawMulAlphaColor( g_object[HEAD_SEA].m_model, TRUE ) ;
	// --- モデルの詳細設定
	MV1SetScale( g_object[HEAD_SEA].m_model, VGet(80.0f, 10.0f, 80.0f) ) ;				// 大きさ
	MV1SetPosition( g_object[HEAD_SEA].m_model, VGet(0.0f, SEAHEIGHT, 0.0f) ) ;			// 座標
	// 画像を読み込む際にアルファ値をRGB値に乗算するように設定する
	SetUsePremulAlphaConvertLoad( TRUE ) ;
	// 画面取り込み用のソフトウエア用画像を作成
	SoftImageHandle = MakeARGB8ColorSoftImage( 640, 480 ) ;

	// シェイプの番号を取得 出来なければ-1
	shape[0] = MV1SearchShape( g_object[HEAD_SEA].m_model, "キー 1" ) ;
	shape[1] = MV1SearchShape( g_object[HEAD_SEA].m_model, "キー 2" ) ;

	// シェイプの番号が読み込めなかった場合エラーとする
	if ( shape[0] < 0 ) return true ;			
	if ( shape[1] < 0 ) return true ;
	return ( false ) ;
}

int DrawSea( void ) {
	// --- 指定したシェイプの有効値を変更する
	MV1SetShapeRate( g_object[HEAD_SEA].m_model, shape[shapeno], seashapestatus ) ;

	// --- 常に波を動かす
	if ( seashapestatus >= 1.0f ) {
		seamovespeed = -WAVESPEED ;					// スピード値をマイナスに変更
//		searotationspeed = SEAROTATIONSPEED ;		// 回転の値をプラスに変更
	} else if ( seashapestatus <= 0.0f ) {
		seamovespeed = WAVESPEED ;					// 値をプラスに変更
//		searotationspeed = 0 ;						// 回転の値を0に
	}
	// 海のシェイプにスピードを足していく
	seashapestatus += seamovespeed ;
	// 海の回転に角度をつける
/*	searotation += searotationspeed ;
	printf( "%f\n" , searotation) ;
	MV1SetRotationXYZ( g_object[HEAD_SEA].m_model, VGet(0.0f, searotation, 0.0f) );		// 回転
*/
	// 海の透明度を設定
	MV1SetOpacityRate( g_object[HEAD_SEA].m_model, SEA_ALPHA / 255.0f ) ;
	// 海の描画
	MV1DrawModel( g_object[HEAD_SEA].m_model ) ;

	// 描画ブレンドモードを乗算済みアルファにセット
	SetDrawBlendMode( DX_BLENDMODE_PMA_ALPHA, SEA_ALPHA ) ;

	// 描画先の画像をソフトイメージに取得する
	GetDrawScreenSoftImage( 0, 0, 640, 480, SoftImageHandle ) ;

	// 取り込んだソフトイメージを使用して透過ウインドウの状態を更新する
	UpdateLayerdWindowForPremultipliedAlphaSoftImage( SoftImageHandle ) ;

	return( false ) ;
}


int InitBlock( void ) {
	float x_scale = 29.0f ,y_scale = 29.0f ,z_scale = 29.0f ;
	float x_size = 60.0f, z_size = 60.0f ;
	int i = HEAD_BLOCK ;

	for (int y = 0 ; y < MAX_ICE_Y ; y++){
		for (int x = 0 ; x < MAX_ICE_X ; x++){
			if ( i < (HEAD_BLOCK + MAX_BLOCK) ){
				MV1SetScale( g_object[i].m_model, VGet(x_scale, y_scale, z_scale) ) ;
				g_object[i].SetPos( ((float)x * x_size), -120.0f, ((float)y * z_size) ) ;
				i++ ;
			}
		}
	}

	return ( false ) ;
}

// --- マップデータのセット
int SetMapData( void ) {
	for (int y = MAP_OFFSET_Y_MIN ; y < MAP_OFFSET_Y_MAX ; y++ ) {
		for ( int x = MAP_OFFSET_X_MIN ; x < MAP_OFFSET_X_MAX ; x++ ) {
//			g_hitmap[y][x] = 0x00 ;
			g_hitmap[y][x] = 0x10 ;
		}
	}

	return ( false ) ;
}

// --- マップデータの閲覧( コンソール表示 : 10進数 )
int LookMapData( void ) {
	printf( "〜マップデータ〜\n" ) ;

	for (int y = MAP_Y_MIN ; y < MAP_Y_MAX ; y++ ) {
		for ( int x = MAP_X_MIN ; x < MAP_X_MAX ; x++ ) {
			printf( "%4d", g_hitmap[y][x]) ;
		}
		printf( "\n" ) ;
	}

	printf( "\n" ) ;

	return ( false ) ;
}


