/* ===================================================== *
	FILE : NPC.h
	NAME : Y.Yamashita

 + ------ Explanation of file --------------------------
       
	NPCクラスの宣言部
	Objectクラスより継承
 * ===================================================== */
class NPC {
	public :
		NPC() ;			// コンストラクタ
		~NPC() ;		// デストラクタ
		void init() ;	// 初期セット

		// --- モデル描画関数
		int LoadModel( int selectmodel );					// モデル読みこみ 仮想関数
		int CopyModel( int selectmodel );					// モデルコピー 仮想関数
		int DrawModel();									// モデル描画
		// --- アクション関数
		int Action();										// アクション 仮想関数
		int PosMove( float x, float y, float z ) ;			// 移動関数
		// --- アニメ関数
		int Animation() ;									// アニメーション
		int AnimationModeChange( int anim_mode ) ;			// アニメーションモードチェンジ
		int AnimStopAction() ;								// 特定のアニメーション後に処理をする関数 仮想関数
		// --- セット関数
		int SetPos( float x, float y, float z ) ;			//	セットm_pos	

		// --- モデル変数
		int m_model ;										//	モデルハンドル
		int m_action_no ;									//	アクションナンバー
		VECTOR m_pos ;										//	ベクトルの座標(float型座標)
		VECTOR m_move ;										//	移動量
		unsigned short m_key ;								//	キー情報
		int	m_direction ;									//	回転角度
		// --- ヒットチェック変数
		float m_width ;										//	当たり判定の幅
		float m_height ;									//	当たり判定の高さ
		VECTOR m_centerPos ;								//	当たり判定の中心座標
		// --- アニメ変数
		int m_anim[MAX_NPC_ANIM] ;							// アニメハンドル
		int m_attachidx ;									//	アタッチインデックスを格納
		int m_animmode ;									//	アニメモード
		float m_anim_totaltime ;							//	アニメ総時間格納変数
		float m_playtime ;									//	アニメ進行時間変数
		float m_playspd ;									//	アニメ進行速度変数
		int m_rootflm ;										//	ルートフレーム
		// --- アニメーションフラグ
		int m_animflg ;										//	アニメーション中フラグ

} ;




