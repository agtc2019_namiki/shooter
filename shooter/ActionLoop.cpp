/* ===================================================== *
	FILE : ActionLoop.cpp
	NAME : Y.Yamashita

 + ------ Explanation of file --------------------------
       
	ActionLoop関数の実装部

 * ===================================================== */
// --- 共通ファイル
#include "Common.h"

// --- ここで一旦放置

int ActionLoop(){ 

	// --- ======== アクション ======== ---

	// --- カメラアクション	カメラの位置、注視点、上方向を設定する関数	裏画面描画の前だと失敗する
	g_camera.Action() ;
	// --- プレイヤーアクション
		// --- キー情報格納
	g_key1P = GetJoypadInputState( DX_INPUT_PAD1  ) ;	// 1P
	g_key2P = GetJoypadInputState( DX_INPUT_PAD2  ) ;	// 2P

// --- Debug キー情報格納(PC用)
g_key1P = GetJoypadInputState( DX_INPUT_KEY_PAD1  ) ;	// 1P
printf("g_key1P = %d\n", g_key1P) ;
printf("g_key2P = %d\n", g_key2P) ;

	g_player_1P.Action( g_key1P ) ;
	g_player_2P.Action( g_key2P ) ;
for ( int i = 0 ; i < MAX_NPC ; i++ ){
		g_npc[i].Action() ;
	}


	// --- =========== 描画 =========== ---

	// --- 描画(OBJ)
	for ( int i = HEAD_BLOCK ; i < (HEAD_BLOCK + 54) ; i++ ){
		g_object[i].DrawModel( ) ;		// ステージ(地面)
	}

	// --- 氷山
	g_object[HEAD_ICEBERG].DrawModel( ) ;

	// --- 最背面描画(スカイドーム)
	DrawSea( ) ;
	DrawSkydome( ) ;

/*
	全オブジェクトの描画
	for ( int i = 0 ; i < MAX_OBJECT ; i++ ){
		g_object[i].DrawModel( ) ;
	}
*/

	return (false) ;
}
